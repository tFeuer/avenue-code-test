package utilities;

import java.awt.*;
import java.awt.event.KeyEvent;

public class Keyboard {

    // Robot to my avail
    Robot robot;

    public void pressEnter() {
        try {
            robot = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }
        robot.keyPress(KeyEvent.VK_ENTER);    // press Enter
        robot.keyRelease(KeyEvent.VK_ENTER);  // release Enter
    }
}
