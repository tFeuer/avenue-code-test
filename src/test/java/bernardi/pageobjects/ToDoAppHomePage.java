package bernardi.pageobjects;

import bernardi.framework.core.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ToDoAppHomePage extends BasePage {

    private final By aSignIn = By.id("sign_in");
    private final By inputEmail = By.id("user_email");
    private final By inputPassword = By.id("user_password");
    private final By inputSignIn = By.xpath("//*[@id=\"new_user\"]/input");

    public ToDoAppHomePage(WebDriver driver) {
        super(driver);
    }

    public ToDoAppHomePage loginAsAdmin(String email, String password) {
        driver.findElement(aSignIn).click();

        driver.findElement(inputEmail).sendKeys(email);

        driver.findElement(inputPassword).sendKeys(password);

        driver.findElement(inputSignIn).click();

        return new ToDoAppHomePage(driver);
    }
}
