package bernardi.pageobjects;

import bernardi.framework.core.BasePage;
import bernardi.pageobjects.ToDoAppHomePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ToDoAppMainPage extends BasePage {

    private final By aMyTasks = By.id("my_task");

    public ToDoAppMainPage(WebDriver driver) {
        super(driver);
    }

    public ToDoAppHomePage waitMyTasksToAppear() {
        waitForElementToAppear(aMyTasks);
        return new ToDoAppHomePage(driver);
    }

    public ToDoAppHomePage clickOnTasks() {
        driver.findElement(aMyTasks).click();
        return new ToDoAppHomePage(driver);
    }
}
