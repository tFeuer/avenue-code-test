package bernardi.pageobjects;

import bernardi.framework.core.BasePage;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import utilities.Faker;
import utilities.Keyboard;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ToDoAppTaskPage extends BasePage {

    private String taskDesc;
    private String subtaskDesc;

    void setTaskDesc(String taskDesc) {
        this.taskDesc = taskDesc;
    }

    String getTaskDesc() {
        return this.taskDesc;
    }

    void setSubtaskDesc(String subtaskDesc) {
        this.subtaskDesc = subtaskDesc;
    }

    String getSubtaskDesc() {
        return this.subtaskDesc;
    }

    private final By h1Title = By.xpath("/html/body/div[1]/h1");

    private final By inputNewTask = By.id("new_task");

    private final By spanAddTask = By.xpath("/html/body/div[1]/div[2]/div[1]/form/div[2]/span");

    private final By buttonManageSubtasks = By.xpath("//div[2]/table/tbody/tr/td[4]/button");

    private final By divModalDialog = By.className("modal-dialog");

    private final By inputNewSubtask = By.id("new_sub_task");

    private final By inputDueDate = By.id("dueDate");

    private final By buttonAddSubtask = By.id("add-subtask");

    public ToDoAppTaskPage(WebDriver driver) {
        super(driver);
    }

    public ToDoAppTaskPage showMessage(String msg) {
        Assert.assertEquals(driver.findElement(h1Title).getText(), msg);
        return new ToDoAppTaskPage(driver);
    }

    Faker faker = new Faker();

    public ToDoAppTaskPage typeNewTask() {
        setTaskDesc(faker.givenUsingPlainJava_whenGeneratingRandomStringBounded_thenCorrect());
        driver.findElement(inputNewTask).sendKeys(getTaskDesc());
        return new ToDoAppTaskPage(driver);
    }

    public ToDoAppTaskPage clickAddTask() {
        driver.findElement(spanAddTask).click();
        return new ToDoAppTaskPage(driver);
    }

    public ToDoAppTaskPage checkIfTaskWasAppended() {
        int numOfRow = driver.findElements(By.xpath("//div[2]/table/tbody/tr")).size();

        for (int iRow = 1; iRow <= numOfRow; iRow++) {
            WebElement td = driver.findElement(By.xpath("//div[2]/table/tbody/tr["+iRow+"]/td[2]"));
            if (td.getText().equals(getTaskDesc())) {
                break;
            }
        }

        return new ToDoAppTaskPage(driver);
    }

    public ToDoAppTaskPage pressEnter() {
        Keyboard keyboard = new Keyboard();
        keyboard.pressEnter();
        return new ToDoAppTaskPage(driver);
    }

    public ToDoAppHomePage waitManageMyTasksToAppear() {
        waitForElementToAppear(buttonManageSubtasks);
        return new ToDoAppHomePage(driver);
    }

    public ToDoAppTaskPage clickManageMyTasksToAppear() {
        driver.findElement(buttonManageSubtasks).click();
        return new ToDoAppTaskPage(driver);
    }

    public ToDoAppHomePage waitModalDialogToAppear() {
        waitForElementToAppear(divModalDialog);
        return new ToDoAppHomePage(driver);
    }

    public ToDoAppTaskPage enterSubtaskDescAndDueDate() {
        setSubtaskDesc(faker.givenUsingPlainJava_whenGeneratingRandomStringBounded_thenCorrect());
        String dateInString = new SimpleDateFormat("MM/dd/yyyy").format(new Date());
        driver.findElement(inputNewSubtask).sendKeys(getSubtaskDesc());
        driver.findElement(inputDueDate).clear();
        driver.findElement(inputDueDate).sendKeys(dateInString);
        return new ToDoAppTaskPage(driver);
    }

    public ToDoAppTaskPage clickAddSubtask() {
        driver.findElement(buttonAddSubtask).click();
        return new ToDoAppTaskPage(driver);
    }

    public ToDoAppTaskPage checkIfSubtaskWasAppended() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebElement td = driver.findElement(By.xpath("/html/body/div[4]/div/div/div[2]/div[2]/table/tbody/tr/td[2]"));
        td.getText().equals(getSubtaskDesc());

        return new ToDoAppTaskPage(driver);
    }
}
