package stepdefs;

import bernardi.pageobjects.ToDoAppHomePage;
import bernardi.pageobjects.ToDoAppMainPage;
import bernardi.pageobjects.ToDoAppTaskPage;
import cucumber.api.PendingException;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.managers.ChromeDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class toDoAppSteps {

    WebDriver driver;
    ToDoAppHomePage toDoAppHomePage;
    ToDoAppMainPage toDoAppMainPage;
    ToDoAppTaskPage toDoAppTaskPage;

    @Given("user opens URL {string}")
    public void user_opens_URL(String url) {
        toDoAppHomePage = new ToDoAppHomePage(driver);
        toDoAppHomePage.navigateTo(url);
    }

    @When("perform a login with the email {string} and password {string}")
    public void perform_a_login_with_the_email_and_password(String email, String password) {
        toDoAppHomePage = new ToDoAppHomePage(driver);
        toDoAppHomePage.loginAsAdmin(email, password);
    }

    @Then("should see ‘My Tasks’ link on the NavBar")
    public void should_see_My_Tasks_link_on_the_NavBar() {
        toDoAppMainPage = new ToDoAppMainPage(driver);
        toDoAppMainPage.waitMyTasksToAppear();
    }

    @Given("user enters the 'My Task' page")
    public void user_enters_the_My_Task_page() {
        toDoAppMainPage = new ToDoAppMainPage(driver);
        toDoAppMainPage.clickOnTasks();
    }

    @Then("should see the following message on the top part: Hey {string}")
    public void shouldSeeTheFollowingMessageOnTheTopPartHeyThisIsYourTodoListForToday(String msg) {
        toDoAppTaskPage = new ToDoAppTaskPage(driver);
        toDoAppTaskPage.showMessage(msg);
    }

    @When("typing a new task")
    public void typing_a_new_task() {
        toDoAppTaskPage = new ToDoAppTaskPage(driver);
        toDoAppTaskPage.typeNewTask();
    }

    @And("click on the add task button")
    public void click_on_the_add_task_button() {
        toDoAppTaskPage = new ToDoAppTaskPage(driver);
        toDoAppTaskPage.clickAddTask();
    }

    @Then("the task should be appended on the list of created tasks")
    public void the_task_should_be_appended_on_the_list_of_created_tasks() {
        toDoAppTaskPage = new ToDoAppTaskPage(driver);
        toDoAppTaskPage.checkIfTaskWasAppended();
    }

    @And("hits enter")
    public void hits_enter() {
        toDoAppTaskPage = new ToDoAppTaskPage(driver);
        toDoAppTaskPage.pressEnter();
    }

    @Given("I'm in the ‘My Task’ page")
    public void iMInTheMyTaskPage() {
        user_opens_URL("https://qa-test.avenuecode.com");
        perform_a_login_with_the_email_and_password("felipe.b.silva@outlook.com", "Felip39S");
        should_see_My_Tasks_link_on_the_NavBar();
        user_enters_the_My_Task_page();
        shouldSeeTheFollowingMessageOnTheTopPartHeyThisIsYourTodoListForToday("Felipe Bernardi da Silva's ToDo List");
    }

    @When("I enter a task")
    public void iEnterATask() {
        typing_a_new_task();
        click_on_the_add_task_button();
        the_task_should_be_appended_on_the_list_of_created_tasks();
    }

    @Then("I should see a button labeled as ‘Manage Subtasks’")
    public void iShouldSeeAButtonLabeledAsManageSubtasks() {
        toDoAppTaskPage = new ToDoAppTaskPage(driver);
        toDoAppTaskPage.waitManageMyTasksToAppear();
    }

    @When("I click ‘Manage Subtasks’ button")
    public void iClickManageSubtasksButton() {
        toDoAppTaskPage = new ToDoAppTaskPage(driver);
        toDoAppTaskPage.clickManageMyTasksToAppear();
    }

    @Then("I should see a popup")
    public void iShouldSeeAPopup() {
        toDoAppTaskPage = new ToDoAppTaskPage(driver);
        toDoAppTaskPage.waitModalDialogToAppear();
    }

    @When("I enter the subtask description and due date")
    public void iEnterTheSubtaskDescriptionAndDueDate() {
        toDoAppTaskPage = new ToDoAppTaskPage(driver);
        toDoAppTaskPage.enterSubtaskDescAndDueDate();
    }

    @And("click the ‘Add Subtasks’ button")
    public void clickTheAddSubtasksButton() {
        toDoAppTaskPage = new ToDoAppTaskPage(driver);
        toDoAppTaskPage.clickAddSubtask();
    }

    @Then("the subtask should be appended on the bottom part of the modal dialog")
    public void theSubtaskShouldBeAppendedOnTheBottomPartOfTheModalDialog() {
        toDoAppTaskPage = new ToDoAppTaskPage(driver);
        toDoAppTaskPage.checkIfSubtaskWasAppended();
    }

    @Before
    public void before() {
        System.setProperty("headless", "false"); // You can set this property elsewhere
        String headless = System.getProperty("headless");

        ChromeOptions chromeOptions = new ChromeOptions();
        ChromeDriverManager.chromedriver().setup();

        if("true".equals(headless)) {
            chromeOptions.addArguments("--headless");
            driver = new ChromeDriver(chromeOptions);
        } else {
            chromeOptions.addArguments("start-maximized");
            chromeOptions.addArguments("enable-automation");
            chromeOptions.addArguments("--no-sandbox");
            chromeOptions.addArguments("--disable-infobars");
            chromeOptions.addArguments("--disable-dev-shm-usage");
            chromeOptions.addArguments("--disable-browser-side-navigation");
            chromeOptions.addArguments("--disable-gpu");
            driver = new ChromeDriver(chromeOptions);
        }
    }

    @After
    public void after() {
        if(null != driver) {
            driver.close();
            driver.quit();
        }
    }
}
