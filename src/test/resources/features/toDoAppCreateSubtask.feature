Feature: Create Task
  As a ToDo App user
  I should be able to create a subtask
  So I can break down my tasks into smaller pieces

  Background:
    Given I'm in the ‘My Task’ page
    When I enter a task
    Then I should see a button labeled as ‘Manage Subtasks’

  Scenario: Create Subtask
    When I click ‘Manage Subtasks’ button
    Then I should see a popup
    When I enter the subtask description and due date
    And click the ‘Add Subtasks’ button
    Then the subtask should be appended on the bottom part of the modal dialog