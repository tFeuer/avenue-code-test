Feature: Create Task
  As a ToDo App user
  I should be able to create a task
  So I can manage my tasks

  Background:
    Given user opens URL "https://qa-test.avenuecode.com"
    When perform a login with the email "felipe.b.silva@outlook.com" and password "Felip39S"
    Then should see ‘My Tasks’ link on the NavBar

  Scenario: Create Task - By Clicking on the Add Task Button
    Given user enters the 'My Task' page
#    Then should see the following message on the top part: Hey "Felipe Bernardi da Silva, this is your todo list for today:"
    Then should see the following message on the top part: Hey "Felipe Bernardi da Silva's ToDo List"
    When typing a new task
    And click on the add task button
    Then the task should be appended on the list of created tasks

  Scenario: Create Task - By Hitting Enter
    Given user enters the 'My Task' page
#    Then should see the following message on the top part: Hey "Felipe Bernardi da Silva, this is your todo list for today:"
    Then should see the following message on the top part: Hey "Felipe Bernardi da Silva's ToDo List"
    When typing a new task
    And hits enter
    Then the task should be appended on the list of created tasks